# k8s-deploy

Simple tool to deploy a basic Kubernetes cluster to a vSphere environment

## Requirements

- VMware vSphere environment with API access
- ISO for template in datastore ahead of time

### Control (local) Node

- `Ansible 2.9` via pip (VMware inventory plugin will not work otherwise)
- `pyVmomi` via pip
- `Jinja2` via pip
- `client` via pip
- `psphere`
- `python-apt` (debian based)

## Usage
Copy `hidden_vars.template.yml` and `vmware.template.ini` and fill out the blanks
Fill out information in `deploy/host_vars/all`

### Running playbooks
`ansible-playbook -i localhost -K 0-prepare.yml`

`ansible-playbook -e "@hidden_vars.yml" -i localhost 1-createTemplates.yml`

`ansible-playbook -e "@hidden_vars.yml" -i localhost 2-createNodes.yml`

`ansible-playbook -e "@hidden_vars.yml" -i vmware.py 3-configureNodes.yml`

### NOTES
Only works for ubuntu currently
Only works for one master node currently